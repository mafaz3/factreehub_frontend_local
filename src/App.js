import './App.css';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Chat from './components/chat/chat';
import AdminFeedback from './components/admin-feedback/admin-feedback';
import ServiceProviderFeedback from './components/service-provider-feedback/service-provider-feedback';
import CustomerFeedback from './components/customer-feedback/customer-feedback';

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/chat" element={<Chat />} />
          <Route path="/adminFeedback" element={<AdminFeedback />} />
          <Route path="/serviceProviderFeedback" element={<ServiceProviderFeedback />} />
          <Route path="/customerFeedBack" element={<CustomerFeedback />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
