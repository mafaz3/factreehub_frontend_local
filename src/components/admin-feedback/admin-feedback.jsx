import React, { useEffect, useState } from 'react';
import '../../commonCss/feedback.css';
import dummyImg from '../../Asset/Image/dummy.jpg';
import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHighlighter } from '@fortawesome/free-solid-svg-icons';
import { faComment } from '@fortawesome/free-regular-svg-icons';

function AdminFeedback() {
    const [feedbackData, setFeedbackData] = useState([]);
    const [newMessage, setNewMessage] = useState("");

    useEffect(() => {
        axios.get('http://localhost:4000/feedback/getAllFeedback')
            .then((response) => {
                setFeedbackData(response.data);
            })
            .catch((error) => {
                console.error('Error fetching feedback data:', error);
            });
    }, []);

    const handleInputChange = (e) => {
        setNewMessage(e.target.value);
    };

    const sendMessage = () => {
        let message = newMessage.trim();

        if (message) {
            let apiEndpoint;

            if (message.includes('@serviceprovider')) {
                apiEndpoint = 'http://localhost:4000/feedback/adminServiceProviderFeedback';
                message = message.replace('@serviceprovider', '');
            } else if (message.includes('@customer')) {
                apiEndpoint = 'http://localhost:4000/feedback/adminCustomerFeedback';
                message = message.replace('@customer', '');
            } else {
                console.error('Invalid message format');
                return;
            }

            const requestData = {
                message: message,
                authorEmail: 'test@admin1.com',
                authRole: 'Admin',
            };

            axios
                .post(apiEndpoint, requestData)
                .then((response) => {
                    console.log(`Message sent to ${apiEndpoint}:`, response.data);
                    setFeedbackData([...feedbackData, response.data]);
                })
                .catch((error) => {
                    console.error(`Error sending message to ${apiEndpoint}:`, error);
                });

            setNewMessage("");
        }
    };



    return (
        <div>
            <div className='container'>
                <div className='pt-5 pb-3'>
                    <a href="" className='text-decoration-none text-dark'>&lt; Back</a>
                </div>
                <div className='row'>
                    <div className='col-6 text-blue'><h5>Quotation file name</h5></div>
                    <div className='col-6 text-end'><button className='btn bg-blue text-white rounded-0'>Download Quotation</button></div>
                </div>
                <hr />
                <div className="row mt-5 mb-4">
                    <div className="col-9">
                        <div className='text-end'>
                            <button className='btn rounded-0 border text-blue fw-bold'><FontAwesomeIcon icon={faHighlighter} /> Highlight</button>
                        </div>
                        <div className='mt-4'>
                            <img src={dummyImg} width='100%' />
                        </div>
                    </div>
                    <div className="col-3">
                        <div>
                            <input type="text" className='w-100 border-gray form-control rounded-0 place-holder text-blue' placeholder='Search comments..' />
                        </div>
                        <div className='mt-4 height overflow-y-scroll overflow-x-hidden scrolling'>
                            {feedbackData.map((item, index) => (
                                <div key={index} className={`card p-3 border-0 rounded-1 mb-2 ${item.authRole === 'Admin' ? 'bg-mate-blue' : item.authRole === 'Service provider' ? 'bg-mate-red' : 'bg-mate-green'}`}>
                                    <p className='small'>{item.message}</p>
                                    <div className="row">
                                        <small className={`col ${item.authRole === 'Admin' ? 'text-mate-blue' : item.authRole === 'Service provider' ? 'text-mate-red' : 'text-mate-green'}`}>{item.authRole}</small>
                                        <small className='col text-end'>{item.date}</small>
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div className='mt-4'>
                            {/* <button className='btn rounded-0 border text-blue fw-bold w-100 text-start'><FontAwesomeIcon icon={faComment} /> Add Comment</button> */}
                            <div className='d-flex border ps-2 pe-2'>
                                <div className='faComment text-blue'>
                                    <FontAwesomeIcon icon={faComment} />
                                </div>
                                <div className='w-100'>
                                    <textarea
                                        className="border-0 add-comment text-blue form-control scrolling-add-comment place-holder"
                                        placeholder="Add Comment"
                                        rows="1"
                                        value={newMessage}
                                        onChange={handleInputChange}
                                        onKeyDown={(e) => {
                                            if (e.key === 'Enter' && !e.shiftKey) {
                                                e.preventDefault();
                                                sendMessage();
                                            }
                                        }}
                                    ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default AdminFeedback