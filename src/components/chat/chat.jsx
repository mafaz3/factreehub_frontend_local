import React from 'react';
import './chat.css';
import { useForm } from 'react-hook-form';
import axios from 'axios';

function Chat() {
    const { register, handleSubmit, formState: { errors, isValid }, trigger, reset } = useForm();

    const onSubmit = (data) => {
        axios.post('http://localhost:4000/api/sendMail', data)
            .then((response) => {
                console.log(response.data);
                // reset();
            })
            .catch((error) => {
                console.error('Error submitting form:', error);
            });
        console.log(data);
        reset();
    };

    return (
        <div className='container'>
            <div className='button text-center m-5'>
                <button type="button" className="btn bg-blue text-white" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Chat Support
                </button>
            </div>

            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header bg-blue text-white">
                            <h1 className="modal-title fs-5" id="exampleModalLabel">Chat Support</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <p>Lorem ipsum dolor sit amet consectetur. Vitae nunc in at sit eget.</p>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="mb-3">
                                    <label htmlFor="name" className='mb-1'>Name</label>
                                    <input type="text" className="form-control rounded-1" id="name" placeholder="Enter your name"
                                        {...register('name', { required: 'Name is required' })}
                                        onBlur={() => {
                                            trigger('name');
                                        }} />
                                    {errors.name && <p className='text-danger'>{errors.name.message}</p>}
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="email" className='mb-1'>Email</label>
                                    <input type="email" className="form-control rounded-1" id="email" placeholder="Enter your email"
                                        {...register('email', {
                                            required: 'Email is required',
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                message: 'Invalid email address',
                                            },
                                        })}
                                        onBlur={() => {
                                            trigger('email');
                                        }}
                                    />
                                    {errors.email && <p className='text-danger'>{errors.email.message}</p>}
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="subject" className='mb-1'>Subject</label>
                                    <input type="text" className="form-control rounded-1" id="subject" placeholder="Enter your subject"
                                        {...register('subject', { required: 'Subject is required' })}
                                        onBlur={() => {
                                            trigger('subject');
                                        }} />
                                    {errors.subject && <p className='text-danger'>{errors.subject.message}</p>}
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="message" className='mb-1'>Message</label>
                                    <textarea className="form-control" id="message" rows="3" placeholder='Type here'
                                        {...register('message', { required: 'Message is required' })}
                                        onBlur={() => {
                                            trigger('message');
                                        }} ></textarea>
                                    {errors.message && <p className='text-danger'>{errors.message.message}</p>}
                                </div>
                                <div className="modal-footer">
                                    <div className='mx-auto'>
                                        <button type="button" className="btn rounded-1 border-blue text-blue m-1 widthBtn" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" className="btn rounded-1 bg-green text-white m-1 widthBtn" disabled={!isValid}>Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Chat;